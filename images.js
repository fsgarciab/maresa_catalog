const images = {
  brands: {
    chery: {
      cars: {
        'arrizo-3': {
          images: {
            CarLogo: require('./images/chery/arrizo3/logo.png'),
            SectionSpec: [
              require('./images/chery/arrizo3/espec.jpg'),
            ],
            SectionHeight:3106,
            SectionColors: {
              white: require('./images/chery/arrizo3/colors/Blanco.png'),
              black: require('./images/chery/arrizo3/colors/Negro.png'),
              '#B0B2B3': require('./images/chery/arrizo3/colors/Plomo.png'),
              '#CC1C24': require('./images/chery/arrizo3/colors/Rojo.png')
            },
            SectionVideo: require('./images/chery/arrizo3/arrizo3.mp4'),
            BackgroundImageColors: require('./images/chery/arrizo3/backgroudcolors.jpg')
          },
          buttonMoreColor:'#B23633',
          buttonMoreTextColor:'white'
        },
        'chery-q': {
          images: {
            CarLogo: require('./images/chery/cherryq/logo.png'),
            SectionSpec: [
              require('./images/chery/cherryq/espec.jpg')
            ],
            SectionHeight:3101,
            SectionColors: {
              blue: require('./images/chery/cherryq/colors/Azul.png'),
              white: require('./images/chery/cherryq/colors/Blanco.png'),
              '#B0B2B3': require('./images/chery/cherryq/colors/Plata.png')
            },
            SectionVideo: require('./images/chery/cherryq/cheryq.mp4'),
            BackgroundImageColors: require('./images/chery/arrizo3/backgroudcolors.jpg')
          },
          buttonMoreColor:'#B23633',
          buttonMoreTextColor:'white'
        },
        practivan: {
          images: {
            CarLogo: require('./images/chery/practivan/logo.png'),
            SectionSpec: [
              require('./images/chery/practivan/espec.jpg')
            ],
            SectionHeight:3122,
            SectionColors: {
              white: require('./images/chery/practivan/colors/Blanca.png'),
              grey: require('./images/chery/practivan/colors/Gris.png'),
              '#B0B2B3': require('./images/chery/practivan/colors/Plomo.png')
            },
            SectionVideo: require('./images/chery/practivan/practivan.mp4'),
            BackgroundImageColors: require('./images/chery/arrizo3/backgroudcolors.jpg')
          },
          buttonMoreColor:'#B23633',
          buttonMoreTextColor:'white'
        },
        'tiggo-2': {
          images: {
            CarLogo: require('./images/chery/tiggo2/logo.png'),
            SectionSpec: [
              require('./images/chery/tiggo2/espec.jpg')
            ],
            SectionHeight:3113,
            SectionColors: {
              blue: require('./images/chery/tiggo2/colors/Azul.png'),
              white: require('./images/chery/tiggo2/colors/Blanco.png'),
              gold: require('./images/chery/tiggo2/colors/Dorado.png'),
              orange: require('./images/chery/tiggo2/colors/Naranja.png'),
              black: require('./images/chery/tiggo2/colors/Negro.png'),
              red: require('./images/chery/tiggo2/colors/Rojo.png')
            },
            SectionVideo: require('./images/chery/tiggo2/tiggo2.mp4'),
            BackgroundImageColors: require('./images/chery/arrizo3/backgroudcolors.jpg')
          },
          buttonMoreColor:'#B23633',
          buttonMoreTextColor:'white'
        },
        'tiggo-3': {
          images: {
            CarLogo: require('./images/chery/tiggo3/logo.png'),
            SectionSpec: [
              require('./images/chery/tiggo3/espec.jpg')
            ],
            SectionHeight:3126,
            SectionColors: {
              '#BAABA0': require('./images/chery/tiggo3/colors/Dorado.png'),
              red: require('./images/chery/tiggo3/colors/Rojo.png')
            },
            SectionVideo: require('./images/chery/tiggo3/tiggo3.mp4'),
            BackgroundImageColors: require('./images/chery/arrizo3/backgroudcolors.jpg')
          },
          buttonMoreColor:'#B23633',
          buttonMoreTextColor:'white'
        },
        'tiggo-4': {
          images: {
            CarLogo: require('./images/chery/tiggo4/logo.png'),
            SectionSpec: [
              require('./images/chery/tiggo4/espec.png')
            ],
            SectionHeight:3105,
            SectionColors: {
              white: require('./images/chery/tiggo4/colors/Blanco.png'),
              black: require('./images/chery/tiggo4/colors/Negro.png'),
              '#B0B2B3': require('./images/chery/tiggo4/colors/Plata.png'),
              red: require('./images/chery/tiggo4/colors/Rojo.png'),
            },
            SectionVideo: require('./images/chery/tiggo4/tiggo4.mp4'),
            BackgroundImageColors: require('./images/chery/arrizo3/backgroudcolors.jpg')
          },
          buttonMoreColor:'#B23633',
          buttonMoreTextColor:'white'
        },
        'tiggo-4-turbo': {
          images: {
            CarLogo: require('./images/chery/tiggo4turbo/logo.png'),
            SectionSpec: [
              require('./images/chery/tiggo4turbo/espec.jpg')
            ],
            SectionHeight:3102,
            SectionColors: {
              white: require('./images/chery/tiggo4turbo/colors/Blanco.png'),
              black: require('./images/chery/tiggo4turbo/colors/Negro.png'),
              red: require('./images/chery/tiggo4turbo/colors/Rojo.png'),
              '#B0B2B3':require('./images/chery/tiggo4turbo/colors/Plata.png'),
            },
            SectionVideo: require('./images/chery/tiggo4turbo/tiggo4.mp4'),
            BackgroundImageColors: require('./images/chery/arrizo3/backgroudcolors.jpg')
          },
          buttonMoreColor:'#B23633',
          buttonMoreTextColor:'white'
        },
        'tiggo-7': {
          images: {
            CarLogo: require('./images/chery/tiggo7/logo.png'),
            SectionSpec: [
              require('./images/chery/tiggo7/espec.jpg')
            ],
            SectionHeight:3089,
            SectionColors: {
              white: require('./images/chery/tiggo7/colors/Blanco.png'),
              black: require('./images/chery/tiggo7/colors/Negro.png'),
              red: require('./images/chery/tiggo7/colors/Rojo.png'),
              '#403933': require('./images/chery/tiggo7/colors/Cafe.png'),
            },
            SectionVideo: require('./images/chery/tiggo7/tiggo7.mp4'),
            BackgroundImageColors: require('./images/chery/arrizo3/backgroudcolors.jpg')
          },
          buttonMoreColor:'#B23633',
          buttonMoreTextColor:'white'
        }
      }
    },
    mazda: {
      cars: {
        'bt50': {
          images: {
            CarLogo: require('./images/mazda/bt50/logo.png'),
            SectionSpec: [
              require('./images/mazda/bt50/espec.jpg'),
            ],
            SectionHeight:3106,
            SectionColors: {
              white: require('./images/mazda/bt50/colors/Blanco.jpg'),
              black: require('./images/mazda/bt50/colors/Negro.jpg'),
              '#B0B2B3': require('./images/mazda/bt50/colors/Aluminio.jpg'),
              '#CC1C24': require('./images/mazda/bt50/colors/Rojo.jpg'),
              'blue': require('./images/mazda/bt50/colors/Azul.jpg'),
              '#423630': require('./images/mazda/bt50/colors/Cafe.jpg'),
            },
            SectionVideo: require('./images/mazda/bt50/bt50.mp4'),
            BackgroundImageColors: require('./images/chery/arrizo3/backgroudcolors.jpg')
          },
          buttonMoreColor:'black',
          buttonMoreTextColor:'white'
        },
        'cx3': {
          images: {
            CarLogo: require('./images/mazda/cx3/logo.png'),
            SectionSpec: [
              require('./images/mazda/cx3/espec.jpg')
            ],
            SectionHeight:3101,
            SectionColors: {
              blue: require('./images/mazda/cx3/colors/Azul.jpg'),
              white: require('./images/mazda/cx3/colors/Blanco.jpg'),
              '#B0B2B3': require('./images/mazda/cx3/colors/Plomo.jpg'),
              '#50759C': require('./images/mazda/cx3/colors/AzulClaro.jpg'),
              '#D6D7DB': require('./images/mazda/cx3/colors/BlancoPerlado.jpg'),
              '#403933': require('./images/mazda/cx3/colors/Cafe.jpg'),
              'grey': require('./images/mazda/cx3/colors/Gris.jpg'),
              'black': require('./images/mazda/cx3/colors/Negro.jpg'),
              'red': require('./images/mazda/cx3/colors/Rojo.jpg'),
            },
            SectionVideo: require('./images/mazda/cx3/cx3.mp4'),
            BackgroundImageColors: require('./images/chery/arrizo3/backgroudcolors.jpg')
          },
          buttonMoreColor:'#B23633',
          buttonMoreTextColor:'white'
        },
        'cx5': {
          images: {
            CarLogo: require('./images/mazda/cx5/logo.png'),
            SectionSpec: [
              require('./images/mazda/cx5/espec.jpg')
            ],
            SectionHeight:3122,
            SectionColors: {
              blue: require('./images/mazda/cx5/colors/Azul.jpg'),
              white: require('./images/mazda/cx5/colors/Blanco.jpg'),
              '#B0B2B3': require('./images/mazda/cx5/colors/Plomo.jpg'),
              '#50759C': require('./images/mazda/cx5/colors/AzulClaro.jpg'),
              'grey': require('./images/mazda/cx5/colors/Gris.jpg'),
              'black': require('./images/mazda/cx5/colors/Negro.jpg'),
              'red': require('./images/mazda/cx5/colors/Rojo.jpg'),
            },
            SectionVideo: require('./images/mazda/cx5/cx5.mp4'),
            BackgroundImageColors: require('./images/chery/arrizo3/backgroudcolors.jpg')
          },
          buttonMoreColor:'#B23633',
          buttonMoreTextColor:'white'
        },
        'cx9': {
          images: {
            CarLogo: require('./images/mazda/cx9/logo.png'),
            SectionSpec: [
              require('./images/mazda/cx9/espec.jpg')
            ],
            SectionHeight:3113,
            SectionColors: {
              blue: require('./images/mazda/cx9/colors/Azul.jpg'),
              white: require('./images/mazda/cx9/colors/Blanco.jpg'),
              '#B0B2B3': require('./images/mazda/cx9/colors/Plomo.jpg'),
              '#403933': require('./images/mazda/cx9/colors/Cafe.jpg'),
              'grey': require('./images/mazda/cx9/colors/Gris.jpg'),
              'black': require('./images/mazda/cx9/colors/Negro.jpg'),
              'red': require('./images/mazda/cx9/colors/Rojo.jpg'),
            },
            SectionVideo: require('./images/mazda/cx9/cx9.mp4'),
            BackgroundImageColors: require('./images/chery/arrizo3/backgroudcolors.jpg')
          },
          buttonMoreColor:'#B23633',
          buttonMoreTextColor:'white'
        },
        'mazda2': {
          images: {
            CarLogo: require('./images/mazda/mazda2/logo.png'),
            SectionSpec: [
              require('./images/mazda/mazda2/espec.jpg')
            ],
            SectionHeight:3126,
            SectionColors: {
              blue: require('./images/mazda/mazda2/colors/Azul.jpg'),
              white: require('./images/mazda/mazda2/colors/Blanco.jpg'),
              '#B0B2B3': require('./images/mazda/mazda2/colors/Plomo.jpg'),
              'grey': require('./images/mazda/mazda2/colors/Grey.jpg'),
              'black': require('./images/mazda/mazda2/colors/Negro.jpg'),
              'red': require('./images/mazda/mazda2/colors/Rojo.jpg'),
            },
            SectionVideo: require('./images/mazda/mazda2/mazda2.mp4'),
            BackgroundImageColors: require('./images/chery/arrizo3/backgroudcolors.jpg')
          },
          buttonMoreColor:'#B23633',
          buttonMoreTextColor:'white'
        },
        'mazda3': {
          images: {
            CarLogo: require('./images/mazda/mazda3/logo.png'),
            SectionSpec: [
              require('./images/mazda/mazda3/espec.jpg')
            ],
            SectionHeight:3105,
            SectionColors: {
              '#747472': require('./images/mazda/mazda3/colors/Plomo.jpg'),
              white: require('./images/mazda/mazda3/colors/Blanco.jpg'),
              'grey': require('./images/mazda/mazda3/colors/Gris.jpg'),
              'black': require('./images/mazda/mazda3/colors/Negro.jpg'),
              'red': require('./images/mazda/mazda3/colors/Rojo.jpg'),
            },
            SectionVideo: require('./images/mazda/mazda3/mazda3.mp4'),
            BackgroundImageColors: require('./images/chery/arrizo3/backgroudcolors.jpg')
          },
          buttonMoreColor:'#B23633',
          buttonMoreTextColor:'white'
        },
        'mazda6': {
          images: {
            CarLogo: require('./images/mazda/mazda6/logo.png'),
            SectionSpec: [
              require('./images/mazda/mazda6/espec.jpg')
            ],
            SectionHeight:3102,
            SectionColors: {
              white: require('./images/mazda/mazda6/colors/Blanco.jpg'),
              'grey': require('./images/mazda/mazda6/colors/Gris.jpg'),
              'black': require('./images/mazda/mazda6/colors/Negro.jpg'),
              'red': require('./images/mazda/mazda6/colors/Rojo.jpg'),
              '#747472': require('./images/mazda/mazda6/colors/Plomo.jpg'),
            },
            SectionVideo: require('./images/mazda/mazda6/mazda6.mp4'),
            BackgroundImageColors: require('./images/chery/arrizo3/backgroudcolors.jpg')
          },
          buttonMoreColor:'#B23633',
          buttonMoreTextColor:'white'
        },
        'mx5': {
          images: {
            CarLogo: require('./images/mazda/mx5/logo.png'),
            SectionSpec: [
              require('./images/mazda/mx5/espec.jpg')
            ],
            SectionHeight:2604,
            SectionColors: {
              white: require('./images/mazda/mx5/colors/Blanco.jpg'),
              black: require('./images/mazda/mx5/colors/Negro.jpg'),
              red: require('./images/mazda/mx5/colors/Rojo.jpg'),
              grey: require('./images/mazda/mx5/colors/Gris.jpg'),
            },
            SectionVideo: require('./images/mazda/mx5/mx5.mp4'),
            BackgroundImageColors: require('./images/chery/arrizo3/backgroudcolors.jpg')
          },
          buttonMoreColor:'#B23633',
          buttonMoreTextColor:'white'
        }
      }
    }
  }
}
export default images
