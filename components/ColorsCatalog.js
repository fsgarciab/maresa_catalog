import React from 'react';
import {View, TouchableOpacity, StyleSheet, Image} from 'react-native';
import {ImageFullSize} from './images/ImageFullSize';
import Dimensions from 'Dimensions';
const {width, height} = Dimensions.get('window');

export default class ColorsCatalog extends React.Component{

    state = {
        colorSelected:"",
        imageSelected:null,
        colorsArray:[]
    };

    _changeImage = (item)=>{
        this.setState({imageSelected:item.image});
    };

    componentDidMount(){
        const {
            selectedCar 
        } = this.props;

        const colors =selectedCar.images.SectionColors;

        const keys = Object.keys(colors)
        let colorsArray =  [];

        for (const key of keys) {
            colorsArray.push({
                color:key,
                image:colors[key]
            });
        }

        const firstColor = colorsArray[0];

        this.setState({colorsArray:colorsArray, imageSelected:firstColor.image, colorSelected:firstColor.color});

    }

    render(){
        const {
            selectedCar 
        } = this.props;
        return (
            <View style={styles.container}>

    
                <View
                    style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        width: width,
                        height: height,
                        backgroundColor:'yellow'
                    }}
                    >
                    <Image
                        style={{
                        height: height, 
                        width : width
                        }}
                        source={selectedCar.images.BackgroundImageColors}
                        
                    />
                </View> 
                {
                    this.state.imageSelected &&
                    < ImageFullSize source ={this.state.imageSelected} height={height*0.70} width={width*0.70}/>
                }
                
                <View style={styles.controlsContainer}>
                    {
                        this.state.colorsArray.map((item)=>{
                            return (
                                <TouchableOpacity
                                style={{
                                    borderWidth:1,
                                    borderColor:'rgba(0,0,0,0.2)',
                                    alignItems:'center',
                                    justifyContent:'center',
                                    width:50,
                                    height:50,
                                    backgroundColor:item.color,
                                    borderRadius:100,
                                    marginLeft:40,
                                    marginRight:40,
                                }}
                                key={'button_'+item.color}
                                onPress={()=>{this._changeImage(item)}}
                            >

                            </TouchableOpacity>
                            )
                        })
                    }

                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    container :{
        alignItems: 'center',
        justifyContent:'center',
        paddingBottom: 80,
        paddingTop: 40,
        backgroundColor:'green'
    },
    car:{

    },
    controlsContainer:{
        flexDirection: 'row',
        justifyContent:'space-between',
        flexGrow: 1,
        marginTop: 0,
    }
});