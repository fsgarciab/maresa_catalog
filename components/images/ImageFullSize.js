import React from 'react';
import {Image} from 'react-native';

 class ImageFullSize extends React.Component{

    render(){
        const{
            source,
            height ,
            width
        } = this.props;

        return(
            <Image source ={source} style={{height:height, width:width, flexGrow:1}} resizeMode="contain"/>
        )
    }
}

export {ImageFullSize}