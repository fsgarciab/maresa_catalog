import React from 'react';
import {
    View,
StyleSheet
} from 'react-native';
import { ImageFullSize } from './images';
import Swiper from 'react-native-swiper';

export default class SectionSlide extends React.Component{
    styles = StyleSheet.create({
        SectionSlide:{
            height:this.props.height,
            backgroundColor:'red',
        }
    });

    render(){
        const {
            sources ,
            height
        } = this.props;

        let swiper = (
            <Swiper dotColor='white' activeDotColor='red'>
                {
                    sources.map((item, index)=>{
                        return (<ImageFullSize source={item} key = {index} height = {height} />)
                    })
                }
            </Swiper>
        ); 

        return (
            <View style={this.styles.SectionSlide}>
               {swiper}
            </View>
        )
    }
}
