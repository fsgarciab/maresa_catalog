import React from 'react';
import {
    TouchableOpacity,
    View,
    Text,
} from 'react-native'

export default class FlatListItem extends React.Component {
    _onPress = () => {
      this.props.onPressItem(this.props.id);
    };
  
    render() {
      const textColor = this.props.selected ? "red" : "black";
      return (
        <TouchableOpacity onPress={this._onPress}>
          <View>
            <Text style={{ color: textColor, padding:10, borderWidth:2, borderColor:'blue' }}>
              {this.props.title}
            </Text>
          </View>
        </TouchableOpacity>
      );
    }
  }