import React from 'react';
import {View, StyleSheet, StatusBar, ScrollView, TouchableOpacity, Text} from 'react-native';
import Dimensions from 'Dimensions';
const {width, height} = Dimensions.get('window');
import images from '../images'
import { Video } from 'expo';
import {ImageFullSize} from './images/ImageFullSize'
import { NavigationEvents } from 'react-navigation';

export default class CarScreen extends React.Component{
    constructor(props){
        super(props);
        this.scrollView = null;
    }

    _onClickMore= () =>{
        const { navigation } = this.props;
        const selectedCar = navigation.getParam('selectedCar', null);
        this.props.navigation.navigate('Spec', {
            selectedCar: selectedCar,
          });
    }

    _onFocus = payload =>{
        this.scrollView.scrollTo({y: 0});
    }

    render(){
        const { navigation } = this.props;
        const selectedCar = navigation.getParam('selectedCar', null);


        return (
            <ScrollView contentContainerStyle={styles.container} ref = {(item)=>{ this.scrollView = item; }}>
                <NavigationEvents
                    onDidFocus={this._onFocus}
              
                />
                <StatusBar hidden />
                <View style={styles.carLogo}>
                   <ImageFullSize source={selectedCar.images.CarLogo} height={undefined} width={'90%'} />
                </View>

                <Video
                    shouldPlay =  {true}
                    resizeMode={ Video.RESIZE_MODE_CONTAIN}
                    source={selectedCar.images.SectionVideo} 
                    isMuted= {true}
                    volume={0}
                    playThroughEarpieceAndroid={false}
                    isLooping={true}
                    style={{
                        width: width,
                        height: height,
                    }}

                />

                 <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        style={{
                            alignItems: 'center',
                            backgroundColor: selectedCar.buttonMoreColor,
                            padding: 20,
                            width:200,
                        }}
                        onPress={this._onClickMore}
                        >
                        <Text style={{color:selectedCar.buttonMoreTextColor}}> CONOCE MÁS </Text>
                    </TouchableOpacity>
                </View>
           </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
  container: {
      flexGrow:1,
      justifyContent: 'center',
      backgroundColor:'black',
  },
  carLogo:{
      height:120,
      justifyContent:'center',
      padding: 0,
      alignItems:'center'
  },
  
  buttonContainer:{
    height:60,
    padding: 0,
    marginTop: -350,
    marginBottom:300,
    flexDirection:'column',
    alignItems: 'flex-end',
  }
});