import React from 'react';
import {ScrollView, FlatList, StyleSheet, StatusBar} from 'react-native';
import Dimensions from 'Dimensions';
const {width, height} = Dimensions.get('window');
import FlatListItem from './FlatListItem'
import images from '../images'

export default class MenuScreen extends React.Component{

    static navigationOptions = {
        title: 'Elija un modelo de vehículo',
        headerStyle: {
            backgroundColor: 'white',
          },
          headerTintColor: '#000',
      };
    state = {
        selected: new Map(),
        brand:"chery"
    };

 
    _keyExtractor = (item, index) => item.id;

    _onPressItem = (id) => {
        // updater functions are preferred for transactional updates
        this.setState((state) => {
        // copy the map rather than modifying state.
            const selected = new Map(state.selected);
            selected.set(id, !selected.get(id)); // toggle
            return {selected};
        });

        const selectedBrand = this.state.brand;

        const selectedCar = images.brands[selectedBrand].cars[id];

        this.props.navigation.navigate('Car', {
            selectedCar: selectedCar,
          });

    };

    _renderItem = ({item}) => (
        <FlatListItem
            id={item.id}
            onPressItem={this._onPressItem}
            selected={!!this.state.selected.get(item.id)}
            title={item.title}
        />
    );

    render(){
        const brand = this.state.brand;
        const cars = images.brands[brand].cars;

        const keys = Object.keys(cars)
        let carsArray =  [];

        for (const key of keys) {
            carsArray.push({
                id:key,
                title:key
            });
        }

        return (
            <ScrollView contentContainerStyle={styles.container}>
                <StatusBar hidden />
                <FlatList
                    data={carsArray}
                    extraData={this.state}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._renderItem}
                />
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexGrow:1,
        justifyContent: 'center',
        backgroundColor:'white',
    }
  });