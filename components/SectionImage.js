import React from 'react';
import {
    View,
    StyleSheet
} from 'react-native';

import {ImageFullSize} from './images';

export default class SectionImage extends React.Component{

    styles = StyleSheet.create({
        SectionPresentation:{
            height:this.props.height,
            backgroundColor:'black',
        }
    });
    render(){
        const {
            source ,
            height
        } = this.props;
        return (
            <View style={this.styles.SectionPresentation}>
                <ImageFullSize source = {source} height = {height}  />
            </View>
        )
    }
}

