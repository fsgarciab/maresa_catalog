import React from 'react';
import { StyleSheet, Text, ScrollView, Image } from 'react-native';
import Dimensions from 'Dimensions';
const {width, height} = Dimensions.get('window');

import SectionImage from './SectionImage'

import ColorsCatalog from './ColorsCatalog';
import InteractionProvider from 'react-native-interaction-provider'
import imagefooter from '../images/footer-banner.jpg'
import PinchZoomView from 'react-native-pinch-zoom-view';

export default class SpecsScreen extends React.Component {

  _backToCarScreen = ()=>{
    this.props.navigation.goBack()
  }


  render() {
    const { navigation } = this.props;
    const selectedCar = navigation.getParam('selectedCar', null);
    return (
      <InteractionProvider
      timeout={20 * 1000} 
      onInactive={() =>{
        this._backToCarScreen()
      } }
      onActive={() => {}}
    >
        <ScrollView contentContainerStyle={styles.container}>
      
          <PinchZoomView minScale={1}>
          {
            selectedCar.images.SectionSpec.map((item, index)=>{
              return  <SectionImage key={index} height= {selectedCar.images.SectionHeight} width = {width} source={item}/>
            })
          }

          <ColorsCatalog selectedCar={selectedCar}/>


          <Image style={{
            height: 90,
            width:width
          }} source={imagefooter}/>
      
          </PinchZoomView>
        </ScrollView>
       
      </InteractionProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
      flexGrow:1,
      justifyContent: 'center',
      backgroundColor:'white',
  }
});
