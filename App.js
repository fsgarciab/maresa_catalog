import React from 'react';
import { createStackNavigator, createSwitchNavigator } from 'react-navigation'; 
import SpecsScreen from './components/SpecsScreen'
import CarScreen from './components/CarScreen'
import MenuScreen from './components/MenuScreen';


const AppStack = createStackNavigator({
    Car: CarScreen,
    Spec: SpecsScreen,
   },
   {
     headerMode: 'none',
   });
const MenuStack = createStackNavigator({ 
    Menu: MenuScreen
 });

const RootStack = createSwitchNavigator(
  {
    App: AppStack,
    Menu: MenuStack
  },
  {
    initialRouteName: 'Menu',
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}